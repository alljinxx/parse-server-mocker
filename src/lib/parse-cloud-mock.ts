import * as ParseClass from 'parse';
import { ParseMock } from './parse.mock';

export class ParseCloudMock {

    afterDelete() {
        return undefined;
    }

    afterSave() {
        return undefined;
    }

    beforeDelete() {
        return undefined;
    }

    beforeSave() {
        return undefined;
    }

    beforeFind() {
        return undefined;
    }

    afterFind() {
        return undefined;
    }

    beforeLogin() {
        return undefined;
    }

    afterLogin() {
        return undefined;
    }

    beforeSaveFile() {
        return undefined;
    }

    afterSaveFile() {
        return undefined;
    }

    beforeDeleteFile() {
        return undefined;
    }

    afterDeleteFile() {
        return undefined;
    }

    define(name: string) {
        if (ParseMock.debugEnabled) {
            console.debug(`Cloud.define(${name}`);
        }
    }

    async run(name: string, data?: ParseClass.Cloud.Params, options?: ParseClass.Cloud.RunOptions): Promise<any> {
        const res = await (ParseMock.instance.mockedCloudRun ? ParseMock.instance.mockedCloudRun(name, data, options) : Promise.resolve({}));
        if (ParseMock.debugEnabled) {
            console.debug(`Cloud.run(${name} ,`, data, options, ') => ', res);
        }
        return res;
    }
}