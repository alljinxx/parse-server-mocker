import * as ParseClass from 'parse';
import { parseMockSrv } from './parse-mock.service';
import { ParseMock } from './parse.mock';
import { ExtendedParseObject, undirtyObject } from './utils';

export class ParseObjectMock<
  T extends ParseClass.Attributes = ParseClass.Attributes
> extends ParseClass.Object<T> {
  static override async pinAll(): Promise<void> {
    return;
  }

  static override async pinAllWithName(): Promise<void> {
    return;
  }

  static override async unPinAll(): Promise<void> {
    return;
  }

  static override async unPinAllObjects(): Promise<void> {
    return;
  }

  static override async unPinAllObjectsWithName(): Promise<void> {
    return;
  }

  static override async unPinAllWithName(): Promise<void> {
    return;
  }

  override async pin(): Promise<void> {
    this.debug('pin');
    return;
  }

  override async unPin(): Promise<void> {
    this.debug('unPin');
    return;
  }

  constructor(className?: string, attributes?: T, options?: unknown) {
    super(className, attributes, options);
  }

  private debug(methodName: string, args?: Array<unknown>): void {
    if (ParseMock.debugEnabled) {
      console.debug(
        this,
        `.${methodName}(`,
        ...parseMockSrv.prepareArgsForDebug(args),
        ')'
      );
    }
  }
}

export function mockObjectController() {
  const controller = ParseMock.instance.CoreManager.get('ObjectController') as {
    save: (
      target: ParseClass.Object | Array<ParseClass.Object>
    ) => Promise<unknown>;
    fetch: (target: unknown) => unknown;
    destroy: (target: ParseClass.Object) => unknown;
  };

  controller.save = async (
    target: ParseClass.Object | Array<ParseClass.Object>
  ) => {
    if (Array.isArray(target)) {
      const promises: Array<Promise<ParseClass.Object>> = [];
      for (const elem of target) {
        promises.push(elem.save());
      }
      return Promise.all(promises);
    } else {
      if (!target.id) {
        target.id = (target as ExtendedParseObject)
          ._getId()
          .substring('local'.length);
      }
      if (!Object.prototype.hasOwnProperty.call(target, '_createdAt')) {
        Object.defineProperty(target, 'createdAt', {
          get: function () {
            return this._createdAt;
          },
          set: function (val) {
            this._createdAt = val;
          },
        });
      }
      if (!target['_createdAt']) {
        target['_createdAt'] = new Date();
      }
      target.set('updatedAt', new Date());
      ParseMock.instance.history.push({ source: target, method: 'save' });
      if (ParseMock.debugEnabled) {
        console.debug(target, '.save()');
      }

      // L'élément vient d'être "save", il ne doit plus être dirty
      undirtyObject(target);

      return target;
    }
  };

  controller.fetch = async (target: ParseClass.Object) => {
    ParseMock.instance.history.push({ source: target, method: 'fetch' });
    if (ParseMock.debugEnabled) {
      console.debug(target, '.fetch()');
    }
    target.id = `fetched_at_${new Date()}`;
    return target;
  };

  controller.destroy = async (target: ParseClass.Object) => {
    ParseMock.instance.history.push({ source: target, method: 'destroy' });
    if (ParseMock.debugEnabled) {
      console.debug(target, '.destroy()');
    }
    return target;
  };
}
