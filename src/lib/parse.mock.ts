import * as ParseClass from 'parse';
import { ParseCloudMock } from './parse-cloud-mock';
import { mockFileController } from './parse-file-mock';
import { parseMockSrv } from './parse-mock.service';
import { ParseObjectMock, mockObjectController } from './parse-object-mock';
import { ParseQueryMock } from './parse-query-mock';

export class ParseMock {
  static instance: ParseMock;

  static debugEnabled = false;

  Query: typeof ParseQueryMock;
  Object: typeof ParseObjectMock;
  Cloud: ParseCloudMock;
  Config: typeof ParseConfigMock;

  // Accès au CoreManager de Parse
  CoreManager: { get: (val: string) => unknown };

  // Historique des actions interceptées par le ParseMock et ses composants
  readonly history: Array<{ source: Parse.Object; method: string }>;

  /**
   * Substitution la méthode get
   */
  mockedQueryGet: (
    query: ParseQueryMock,
    objectId: string,
    options?: ParseClass.Query.GetOptions
  ) => Promise<ParseClass.Object>;

  /**
   * Substitution la méthode find
   */
  mockedQueryFind: (
    query: ParseQueryMock,
    options?: ParseClass.Query.FindOptions
  ) => Promise<ParseClass.Object[]>;

  /**
   * Substitution la méthode findAll
   */
  mockedQueryFindAll: (
    query: ParseQueryMock,
    options?: ParseClass.Query.FindOptions
  ) => Promise<ParseClass.Object[]>;

  /**
   * Substitution la méthode first
   */
  mockedQueryFirst: (
    query: ParseQueryMock,
    options?: ParseClass.Query.FindOptions
  ) => Promise<ParseClass.Object | undefined>;

  /**
   * Substitution la méthode count
   */
  mockedQueryCount: (
    query: ParseQueryMock,
    options?: ParseClass.Query.CountOptions
  ) => Promise<number>;

  /**
   * Substitution la méthode each
   */
  mockedQueryEach: (
    query: ParseQueryMock,
    callback: (obj: ParseClass.Object) => PromiseLike<void> | void,
    options?: ParseClass.Query.BatchOptions
  ) => Promise<void>;

  /**
   * Substitution de la méthode Cloud.run
   */
  mockedCloudRun: (
    name: string,
    data?: null | ParseClass.Cloud.Params,
    options?: ParseClass.Cloud.RunOptions
  ) => Promise<unknown>;

  /**
   * Substitution de la configuration de Parse stockée en BDD
   */
  mockedCloudConfig: Record<string, unknown>;

  /**
   * Substitution de l'utilisateur courant loggé dans Parse
   */
  mockedUser: ParseClass.User;
}

export class ParseUserMock<
  T extends ParseClass.Attributes = ParseClass.Attributes
> extends ParseClass.User<T> {
  static override current<T extends ParseClass.User>(): T | undefined {
    return ParseMock.instance.mockedUser as T;
  }

  static override logIn<T extends ParseClass.User>(): Promise<T> {
    return Promise.resolve(ParseMock.instance.mockedUser as T);
  }

  static override logOut<T extends ParseClass.User>(): Promise<T> {
    return Promise.resolve(ParseMock.instance.mockedUser as T);
  }

  static override become<T extends ParseClass.User>(): Promise<T> {
    return Promise.resolve(ParseMock.instance.mockedUser as T);
  }
}

export class ParseConfigMock extends ParseClass.Config {
  private static readonly instance = new ParseConfigMock();

  static override async get(): Promise<ParseConfigMock> {
    ParseConfigMock.instance.debug('get');
    return ParseConfigMock.instance;
  }

  static override current(): ParseConfigMock {
    ParseConfigMock.instance.debug('current');
    return ParseConfigMock.instance;
  }

  static override async save(): Promise<ParseConfigMock> {
    ParseConfigMock.instance.debug('save');
    return ParseConfigMock.instance;
  }

  override get(attr: string): unknown {
    const res = ParseMock.instance.mockedCloudConfig[attr];
    ParseConfigMock.instance.debug('get', [attr], res);
    return res;
  }

  override escape(attr: string): unknown {
    const res = ParseMock.instance.mockedCloudConfig[attr];
    ParseConfigMock.instance.debug('escape', [attr], res);
    return res;
  }

  private debug(
    methodName: string,
    args?: Array<unknown>,
    res?: unknown
  ): void {
    if (ParseMock.debugEnabled) {
      console.debug(
        this,
        `.${methodName}(`,
        ...parseMockSrv.prepareArgsForDebug(args),
        ') => ',
        res
      );
    }
  }
}

export function mockParse(source: unknown): ParseMock {
  ParseMock.instance = source as ParseMock;
  Object.defineProperty(ParseMock.instance, 'Query', {
    get: function () {
      return ParseQueryMock;
    },
  });
  Object.defineProperty(ParseMock.instance, 'Object', {
    get: function () {
      return ParseObjectMock;
    },
  });
  Object.defineProperty(ParseMock.instance, 'Cloud', {
    get: function () {
      return new ParseCloudMock();
    },
  });
  Object.defineProperty(ParseMock.instance, 'Config', {
    get: function () {
      return ParseConfigMock;
    },
  });
  Object.defineProperty(ParseMock.instance, 'User', {
    get: function () {
      return ParseUserMock;
    },
  });
  ParseMock.instance.mockedCloudConfig = {};
  (ParseMock.instance as { history: unknown[] }).history = [];
  (
    ParseMock.instance as unknown as { enableLocalDatastore: () => void }
  ).enableLocalDatastore = () => {
    return;
  };

  // On doit aussi mocker l'ObjectController de Parse, sinon on aura aucun contrôle sur les objets créés (cas du new Object() par exemple)
  mockObjectController();
  mockFileController();

  return ParseMock.instance;
}
