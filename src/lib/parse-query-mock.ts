import * as ParseClass from 'parse';
import { parseMockSrv } from './parse-mock.service';
import { ParseMock } from './parse.mock';

export type WhereClause = {
  [attr: string]: unknown;
};

export type QueryJSON = {
  where: WhereClause;
  include?: string;
  excludeKeys?: string;
  keys?: string;
  limit?: number;
  skip?: number;
  order?: string;
  className?: string;
  count?: number;
  hint?: unknown;
  explain?: boolean;
  readPreference?: string;
  includeReadPreference?: string;
  subqueryReadPreference?: string;
};

export class ParseQueryMock<
  T extends ParseClass.Object = ParseClass.Object
> extends ParseClass.Query<T> {
  static override or<U extends ParseClass.Object>(
    ...queries: Array<ParseClass.Query<U>>
  ): ParseClass.Query<U> {
    const query = new ParseQueryMock(queries[0].className);
    (query as any)._orQuery(queries);
    return query as unknown as ParseClass.Query<U>;
  }

  static override and<U extends ParseClass.Object>(
    ...queries: Array<ParseClass.Query<U>>
  ): ParseClass.Query<U> {
    const query = new ParseQueryMock(queries[0].className);
    (query as any)._andQuery(queries);
    return query as unknown as ParseClass.Query<U>;
  }

  static override nor<U extends ParseClass.Object>(
    ...queries: Array<ParseClass.Query<U>>
  ): ParseClass.Query<U> {
    const query = new ParseQueryMock(queries[0].className);
    (query as any)._norQuery(queries);
    return query as unknown as ParseClass.Query<U>;
  }

  override async get(
    objectId: string,
    options?: ParseClass.Query.GetOptions
  ): Promise<T> {
    const res = ParseMock.instance.mockedQueryGet
      ? (ParseMock.instance.mockedQueryGet(
          this,
          objectId,
          options
        ) as unknown as Promise<T>)
      : undefined;
    this.debug('get', [objectId, options], res);
    return res;
  }

  override async find(options?: ParseClass.Query.FindOptions): Promise<T[]> {
    const res = await (ParseMock.instance.mockedQueryFind
      ? ParseMock.instance.mockedQueryFind(this, options)
      : Promise.resolve([]));
    this.debug('find', [options], res);
    return res;
  }

  override async findAll(options?: ParseClass.Query.FindOptions): Promise<T[]> {
    const res = await (ParseMock.instance.mockedQueryFindAll
      ? ParseMock.instance.mockedQueryFindAll(this, options)
      : Promise.resolve([]));
    this.debug('findAll', [options], res);
    return res;
  }

  override async first(options?: ParseClass.Query.FindOptions): Promise<T> {
    const res = await (ParseMock.instance.mockedQueryFirst
      ? ParseMock.instance.mockedQueryFirst(this, options)
      : Promise.resolve(undefined));
    this.debug('first', [options], res);
    return res;
  }

  override async count(
    options?: ParseClass.Query.CountOptions
  ): Promise<number> {
    const res = await (ParseMock.instance.mockedQueryCount
      ? ParseMock.instance.mockedQueryCount(this, options)
      : Promise.resolve(undefined));
    this.debug('count', [options], res);
    return res;
  }

  override async each(
    callback: (obj: T) => PromiseLike<void> | void,
    options?: ParseClass.Query.BatchOptions
  ): Promise<void> {
    this.debug('each');
    if (ParseMock.instance.mockedQueryEach) {
      await ParseMock.instance.mockedQueryEach(this, callback, options);
    }
  }

  override fromLocalDatastore(): this {
    this.debug('fromLocalDatastore');
    return this;
  }

  private debug(
    methodName: string,
    args?: Array<unknown>,
    res?: unknown
  ): void {
    if (ParseMock.debugEnabled) {
      console.debug(
        this,
        `.${methodName}(`,
        ...parseMockSrv.prepareArgsForDebug(args),
        ') => ',
        res
      );
    }
  }
}

// export function mockQueryController() {
//   const controller = ParseMock.instance.CoreManager.get('QueryController') as {
//     find(
//       className: string,
//       params: QueryJSON,
//       options: ParseClass.RequestOptions
//     ): Promise<Array<Parse.Object>>;
//     aggregate(
//       className: string,
//       params: any,
//       options: ParseClass.RequestOptions
//     ): Promise<Array<unknown>>;
//   };

//   controller.find = async (
//     className: string,
//     params: QueryJSON,
//     options: ParseClass.RequestOptions
//   ) => {
//     console.log(className);
//     console.log(params);
//     console.log(options);
//     return Promise.resolve([]);
//   };
// }
