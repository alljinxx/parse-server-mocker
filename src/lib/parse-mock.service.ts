/**
 * Service principal
 */
class ParseMockService {

    /**
     * Transforme les arguments d'une méthode afin de les rendre affichable dans un console.debug en y insérent des virgules entre chaque
     * @param args Arguments de méthode
     * @returns Arguments formatés
     */
    prepareArgsForDebug(args?: Array<unknown>): Array<unknown> {
        const modifiedArgs = [];
        if (args) {
            for (const arg of args) {
                modifiedArgs.push(arg, ',');
            }
            modifiedArgs.splice(modifiedArgs.length - 1);
        }
        return modifiedArgs;
    }
}

export const parseMockSrv = new ParseMockService();