import * as Parse from 'parse';
import { mockParse, ParseMock } from './parse.mock';

describe('mockParse', () => {
  beforeAll(() => {
    mockParse(Parse);
  });

  it('should create', async () => {
    ParseMock.instance.history.length = 0;
    const obj = new Parse.Object();
    obj.set('msg', 'hello');
    expect(obj.dirty()).toBeTruthy();
    await obj.save();
    expect(ParseMock.instance.history.length).toBe(1);
    expect(obj.id).toBeDefined();
    expect(obj.createdAt).toBeDefined();
    expect(obj.dirty()).toBeFalsy();
  });

  it('ParseUserMock', async () => {
    expect(Parse.User.current()).toBeUndefined();
    const user = new Parse.User({ username: 'bob' });
    ParseMock.instance.mockedUser = user;
    expect(Parse.User.current().getUsername()).toBe('bob');
  });
});
