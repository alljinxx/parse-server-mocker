import * as Parse from 'parse';
import { mockParse, ParseMock } from './parse.mock';

describe('ParseQueryMock', () => {
  beforeAll(() => {
    mockParse(Parse);
  });

  it('mockedQueryFirst', async () => {
    const obj = new Parse.Object('theClass');

    ParseMock.instance.mockedQueryFirst = () => {
      return Promise.resolve(obj);
    };

    const res = await new Parse.Query('').first();
    expect(res).toBe(obj);
  });

  it('mockedQueryOrAndNor', async () => {
    const obj = new Parse.Object('theClass');

    ParseMock.instance.mockedQueryFind = () => {
      return Promise.resolve([obj]);
    };

    const query1 = new Parse.Query('');
    const query2 = new Parse.Query('');

    {
      const compound = Parse.Query.or(query1, query2);
      const res = await compound.find();
      expect(res[0]).toBe(obj);
    }

    {
      const compound = Parse.Query.and(query1, query2);
      const res = await compound.find();
      expect(res[0]).toBe(obj);
    }

    {
      const compound = Parse.Query.nor(query1, query2);
      const res = await compound.find();
      expect(res[0]).toBe(obj);
    }
  });
});
