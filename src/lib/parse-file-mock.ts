import { ParseMock } from './parse.mock';

export function mockFileController() {
  const controller = ParseMock.instance.CoreManager.get('FileController') as {
    saveBase64: (
      name: string,
      source: unknown
    ) => Promise<{
      name: string;
      url: string;
    }>;
  };

  controller.saveBase64 = async (name: string) => {
    return Promise.resolve({
      name,
      url: `http://url.to.mockedFile.${name}`,
    });
  };
}
