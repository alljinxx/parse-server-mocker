import * as ParseClass from 'parse';
import { ParseMock } from './parse.mock';

export type ExtendedParseObject<
  T extends ParseClass.Object = ParseClass.Object
> = T & {
  _localId: string;
  _toFullJSON(
    seen?: unknown[],
    offline?: boolean
  ): ParseClass.Object.ToJSON<Parse.Attributes> & Parse.JSONBaseAttributes;
  _getId(): string;
  toOfflinePointer(): ParseClass.Pointer;
  _finishFetch(serverData: Parse.Attributes): void;
  _clearPendingOps(): void;
  _clearServerData(): void;
  _getStateIdentifier(): string;
};

/**
 * Retourne l'ObjectStateController de Parse
 * @returns
 */
export function getObjectStateController() {
  return ParseMock.instance.CoreManager.get('ObjectStateController') as {
    popPendingState: (
      identifier: string
    ) => Record<string, { applyTo: (attr: unknown) => unknown }>;
    commitServerChanges: (
      identifier: string,
      changes: Record<string, unknown>
    ) => void;
  };
}

export function undirtyObject(obj: ParseClass.Object) {
  // const stateController = (globalParse().CoreManager as ExtendedParseObject).getObjectStateController();
  const pending = getObjectStateController().popPendingState(
    (obj as ExtendedParseObject)._getStateIdentifier()
  );
  const changes: Record<string, unknown> = {};
  for (const attr of Object.keys(pending)) {
    // Only SetOps and UnsetOps should not come back with results
    changes[attr] = pending[attr].applyTo(undefined);
  }

  getObjectStateController().commitServerChanges(
    (obj as ExtendedParseObject)._getStateIdentifier(),
    changes
  );
}
